# Administer A Server Running Debian 10 #

The documentation files are under a subdirectory `doc`.
They are tabulated below in an order in which you could
use them, starting with a fresh install of Debian 10.

| doc | Purpose |
------|---------
| [admin.md](https://bitbucket.org/jack_waugh/adm_debian10_srv/src/master/doc/admin.md) | Establish A User Account for System Administration |
| [tls0.md](https://bitbucket.org/jack_waugh/adm_debian10_srv/src/master/doc/tls0.md) | First part of TLS -- install `certbot` |

To do (in some order):

- Check what DBMS the running system is using.

It is running sqlite3. So, that will be self-contained, and
I don't have to install MySQL.

Shall I get into trouble if I don't configure an FQDN?

- Install nginx and Postfix.
```
sudo apt install -y nginx
sudo apt install -y postfix
sudo apt install -y rsync
sudo apt install -y imagemagick # for Pennstation
# for Ruby:
sudo apt install -y libreadline-dev zlib1g-dev \
  libyaml-dev
sudo apt install -y \
  autoconf bison build-essential \
  libncurses5-dev libffi-dev libgdbm-dev
sudo apt install -y g++ gcc autoconf automake bison \
  libc6-dev \
  libffi-dev libgdbm-dev libncurses5-dev libsqlite3-dev \
  libtool \
  libyaml-dev make pkg-config sqlite3 zlib1g-dev \
  libgmp-dev \
  libreadline-dev
sudo apt-get install -y autoconf bison build-essential \
  libssl-dev libyaml-dev libreadline6-dev zlib1g-dev \
  libncurses5-dev libffi-dev libgdbm6 libgdbm-dev libdb-dev
```
In the installation procedure, Postfix offers some choice
of starting config. I will choose "Internet site: Mail is 
sent and received directly using SMTP."

It's asking for "System mail name", which it explains is
usually the FQDN. I will use the domain name assigned by
Linode, e. g. li243-111.members.linode.com

It can't hurt and it might help if the result from
"hostname -f" agrees.
```
sudo hostnamectl set-hostname li243-111.members.linode.com
```
- Create regular user accounts.
```
set -u
cd /home
umask 2
for u in share knott
do
  sudo adduser $u
  sudo cp -a $u/.bashrc $u/orig.bashrc
  sudo mkdir $u/.ssh
  sudo cp -a /root/.ssh/authorized_keys $u/.ssh
  sudo chown $u $u/.ssh $u/.ssh/*
  sudo chgrp $u $u/.ssh $u/.ssh/*
  sudo chmod 600 $u/.ssh/authorized_keys
  sudo chmod 755 $u/.ssh
done
```
- Provide Ruby.

SSH into user "share" directly from local machine.
```
set -u
umask 2
mkdir projects
cd projects
git clone git@bitbucket.org:jack_waugh/debian10_user.git \
  user
cd user
```
Pick up instructions in that repo.

- Copy over the application.

Achieved, I think.

- Compare the default Postfix config with Warren's and try to guess what may suffice.

- Test the app without nginx, including ability to send mail.

- Take out a TLS cert.

- Configure nginx.

