# Establish A User Account for System Administration #

The author has a bias against doing more than necessary
as the superuser. Instead, he holds, there should be a
user account for administration of the system and it should
be able to use `sudo` when it becomes necessary to run 
commands with superuser powers.

In fact, we will use a special version of `sudo` that
does not take the options that standard `sudo` does, and
that never asks for a password. Avoiding passwords helps
with security, in the opinion of the author.

The system comes with "sys" as a user account, but it
does not allow logins.

As `root` on a newly initialized server:
```
usermod --home /home/sys --shell /bin/bash sys
cd /home
umask 2
mkdir sys
chown sys sys
chgrp sys sys
cp -ar /root/.ssh sys
chown sys `find sys/.ssh -print`
chgrp sys `find sys/.ssh -print`
apt-get install -y git
apt-get install -y cmake
apt-get install -y ed
```
Maybe leave the `root` window open for later.

Try logging in as the `sys` user.
```
set -u
umask 2
echo "$PATH" | tee origpath
mkdir projects
cd projects
git clone \
  https://bitbucket.org/jack_waugh/adm_debian10_srv.git
cd adm_debian10_srv
git config user.email m2hh2kmhsn@snkmail.com
git config user.name "Jack WAUGH"
git pull
cd src
make
```
Back in Superuser window:
```
cd /home/sys/gate
chown root sudo
chgrp root sudo
chmod g+s,u+s sudo
ls -l sudo
```
Test:

Return to your window that is logged in as "sys".

Log out, then log in again.
```
sudo id
```
If it says root, congratulations.
If not, we've had a problem.

