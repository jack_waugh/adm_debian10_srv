# First part of TLS: Installing `certbot` #

TLS is Transport-level Security.

As the `sys` user:
```
sudo apt update
sudo apt upgrade -y # while we are at it
sudo apt install -y snapd
sudo snap install core
sudo snap refresh core
sudo apt-get remove certbot
sudo snap install --classic certbot
```
Log out and log back in, to get updated PATH.
```
certbot --version
```
This is enough for now while we don't even have `nginx`
installed let alone configured, nor Postfix.
