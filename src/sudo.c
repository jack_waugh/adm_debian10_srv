#include <unistd.h>
#include <stdio.h>

/*
	sudo -- Run a command as the superuser.

  This version doesn't take any of the options that the
  standard version takes, and will never ask for a password.
*/

/*
 * main --- main program
 */
int main(int argc, char **argv)
{
	if (argc < 2) {
    write(2, "Usage: sudo cmd {arg}\n", 22);
    return 2;
  }
	(void) setuid((__uid_t) 0);
	(void) setgid((__gid_t) 0);
	if (execvp(argv[1], argv + 1)) {
		perror(argv[1]);
		return 1;
  }
	/* NOTREACHED */
}

